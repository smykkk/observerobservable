package Zadanie3;

import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

public class ServiceDepartment implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        if(arg instanceof Request && ((Request) arg).getRequestType().equals(Request.RequestType.SERVICE)){
            try {
                Database.INSTANCE.addRecord(arg, DatabaseName.DB_SERVICE);
                System.out.println("Request " + ((Request) arg).getContent() + " from "+ ((Request) arg).getUserName() +
                " is being handled by Service Department.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
