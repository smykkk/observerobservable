package Zadanie3;

import lombok.Getter;

import java.io.*;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

public class FinanceDepartment implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Request) {
            try {
                increment(((Request) arg).getRequestType());
                Database.INSTANCE.addRecord(arg, DatabaseName.DB_REQUESTS);
                System.out.println("Request " + ((Request) arg).getContent() + " from " + ((Request) arg).getUserName() +
                " is being recorded by Finance Department.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void getStatistics(){
        System.out.println(FinanceStatistics.INSTANCE.info + " info requests received.");
        System.out.println(FinanceStatistics.INSTANCE.service + " service requests received.");
        System.out.println(FinanceStatistics.INSTANCE.order + " order requests received.");
        System.out.println(FinanceStatistics.INSTANCE.complaint + " complaint requests received.");
    }

    private int increment(Request.RequestType requestType){
        switch (requestType){
            case INFO: return FinanceStatistics.INSTANCE.info++;
            case SERVICE: return FinanceStatistics.INSTANCE.service++;
            case ORDER: return FinanceStatistics.INSTANCE.order++;
            case COMPLAINT: return FinanceStatistics.INSTANCE.complaint++;
            default: throw new IllegalArgumentException("Wrong requestType given.");
        }
    }

    @Getter
    public enum FinanceStatistics{
        INSTANCE;
        private int info = 0;
        private int service = 0;
        private int order = 0;
        private int complaint = 0;

        FinanceStatistics() {
            try {
                updateStats();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        private void updateStats() throws FileNotFoundException {
            Scanner skan = new Scanner(DatabaseName.DB_REQUESTS.getFile());
            while (skan.hasNextLine()){
                String inputLine = skan.nextLine();
                if(inputLine.contains("INFO")) info++;
                else if (inputLine.contains("SERVICE")) service++;
                else if (inputLine.contains("ORDER")) order++;
                else if (inputLine.contains("COMPLAINT")) complaint++;
            }

        }

    }
}
