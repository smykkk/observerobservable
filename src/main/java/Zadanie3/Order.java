package Zadanie3;

import lombok.Getter;

@Getter
public class Order {
    private String content;
    private String userName;

    public Order(String content, String userName) {
        this.content = content;
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "{" + content + '}';
    }

}
