package Zadanie3;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        WebService wb = new WebService();
        Scanner skan = new Scanner(System.in);

        System.out.println("Welcome to our customer service center.");
        System.out.println("Choose one of the following options:\n[new/stats/quit]");
        String inputLine = "";

        while (!inputLine.equals("quit")) {
            inputLine = skan.nextLine();
            if(inputLine.equals("new")){
                processEvent(skan, wb);
            }
            else if (inputLine.equals("stats")){
                wb.getFinanceDepartment().getStatistics();
            }
            else System.out.println("Choose one of the following options:\n[new/stats/quit]");
        }
    }

    public static void processEvent(Scanner skan, WebService wb) {
        try {
            System.out.println("Enter name and surname: ");
            String name = skan.nextLine();
            System.out.println("Enter category [service/order/info/complaint]:");
            Request.RequestType requestType = Request.RequestType.valueOf(skan.nextLine().toUpperCase());
            System.out.println("Enter a short description of what is needed:");
            String content = skan.nextLine();
            wb.processRequest(requestType, content, name);

        } catch (IllegalArgumentException e) {
            System.out.println("Wrong request type. Process restarted. Please choose 'new' option to retry.");
        }
    }
}
