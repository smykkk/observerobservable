package Zadanie3;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Observable;
import java.util.Observer;

public class MarketingDepartment implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        if(arg instanceof Request && ((Request) arg).isOrder()){
            try {
                Order order = new Order(((Request) arg).getContent(), ((Request) arg).getUserName());
                Database.INSTANCE.addRecord(order, DatabaseName.DB_ORDERS);
                System.out.println("Order " + order.getContent()+ " from " + order.getUserName() + " is being handled by Marketing Department");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
