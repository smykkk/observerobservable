package Zadanie3;

import lombok.Getter;

import java.util.Observable;

@Getter

public class WebService extends Observable {

    private FinanceDepartment financeDepartment = new FinanceDepartment();
    private MarketingDepartment marketingDepartment = new MarketingDepartment();
    private ServiceDepartment serviceDepartment = new ServiceDepartment();

    public WebService() {
        super.addObserver(financeDepartment);
        super.addObserver(marketingDepartment);
        super.addObserver(serviceDepartment);
    }

    public void processRequest(Request.RequestType type, String content, String requesterName){
        Request r = new Request(type, content, requesterName);
        setChanged();
        notifyObservers(r);
    }
}
