package Zadanie3;

import lombok.Getter;

@Getter

public class Request {
    private boolean isOrder = false;
    private RequestType requestType;
    private String content;
    private String userName;

    public Request(RequestType requestType, String content, String userName) {
        this.requestType = requestType;
        if(requestType.equals(RequestType.ORDER)) isOrder = true;
        this.content = content;
        this.userName = userName;
    }

    public enum RequestType{
        SERVICE, ORDER, INFO, COMPLAINT
    }

    @Override
    public String toString() {
        return "{requester=" + userName +
                "type=" + requestType +
                " | content='" + content + '\'' +
                '}';
    }
}
