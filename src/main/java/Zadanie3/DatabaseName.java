package Zadanie3;

import lombok.Getter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

@Getter

public enum DatabaseName {
    DB_SERVICE("/Users/marekdybusc/IdeaProjects/Programowanie2Amen/ObserverObservable/src/main/java/Zadanie3/users.txt"),
    DB_ORDERS("/Users/marekdybusc/IdeaProjects/Programowanie2Amen/ObserverObservable/src/main/java/Zadanie3/orders.txt"),
    DB_REQUESTS("/Users/marekdybusc/IdeaProjects/Programowanie2Amen/ObserverObservable/src/main/java/Zadanie3/requests.txt");

    private File file;
    private PrintWriter writer;


    DatabaseName(String path){
        this.file = new File(path);
        try {
            this.writer = new PrintWriter(new FileWriter(file, true));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
