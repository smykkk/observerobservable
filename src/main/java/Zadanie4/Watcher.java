package Zadanie4;


import java.util.Observable;
import java.util.Observer;

public class Watcher implements Observer{

    @Override
    public void update(Observable o, Object arg) {
        if(arg instanceof String){
            System.out.println(arg);
        }
    }
}
