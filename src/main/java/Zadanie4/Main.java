package Zadanie4;

public class Main {
    public static void main(String[] args) {
        NewsStation ns = new NewsStation();
        ns.addObserver(new Watcher());
        ns.sendMessage("thisIsARandomMessage.");
    }
}
