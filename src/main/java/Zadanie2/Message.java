package Zadanie2;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Message {
    private int senderID;
    private String text;
    private boolean isPrivate = false;
    private int receiverID;

    public Message(int senderID, String text) {
        this.senderID = senderID;
        this.text = text;
    }

    public Message(int senderID, String text, boolean isPrivate, int receiverID) {
        this.senderID = senderID;
        this.text = text;
        this.isPrivate = isPrivate;
        this.receiverID = receiverID;
    }
}
