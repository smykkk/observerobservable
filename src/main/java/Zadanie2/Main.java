package Zadanie2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ChatRoom c = new ChatRoom("Pokój rozmów");
        c.userLogin("Dariusz");
        c.userLogin("Grześ");
        c.userLogin("Typasek");
        c.userLogin("Typeska");
        c.userLogin("Alicja");
        c.userLogin("Ozyrys");

        System.out.println("Online:\n" + c.onlineUsers.entrySet());

        Scanner skan = new Scanner(System.in);
        String inputline = "";
        boolean isWorking = true;

        System.out.println("\nEnter your nick to log in:");
        c.userLogin(skan.nextLine());

        while (isWorking) {
            System.out.println("\nEnter message / 'private' <receiverID> <message> / kick <id> if you're admin /'l' to see who's online.");
            inputline = skan.nextLine();
            if (inputline.equals("quit")) isWorking = false;
            else if(inputline.equals("l")) System.out.println("Online:\n" + c.onlineUsers.entrySet());
            else if (inputline.startsWith("private")){

                try {

                    c.sendPrivateMessage(7,
                            inputline.split(" ", 3)[2],
                            Integer.parseInt(inputline.split(" ", 3)[1]));

                } catch (NumberFormatException nfe) {
                    System.out.println("incorrect message format.");
                }
            }
            else if (inputline.startsWith("kick")){
                c.kickOut(Integer.parseInt(inputline.split(" ")[1]), 7);
            }

            else c.sendMessage(7, inputline);
        }
    }
}
