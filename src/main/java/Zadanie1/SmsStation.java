package Zadanie1;

import java.util.Observable;

public class SmsStation extends Observable{


    public void addPhone(Phone phone){
        super.addObserver(phone);
    }

    public void sendSms(int number, String message){
        Message m = new Message(message, number);
        super.setChanged();
        super.notifyObservers(m);
    }

    @Override
    protected synchronized void setChanged() {
        super.setChanged();
    }

    @Override
    public void notifyObservers(Object arg) {
        super.notifyObservers(arg);
    }
}
