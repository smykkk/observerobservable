package Zadanie1;

import lombok.Getter;

@Getter

public class Message {
    private String text;
    private int number;

    public Message(String text, int number) {
        this.text = text;
        this.number = number;
    }
}
