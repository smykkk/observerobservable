Zadanie  1: Stworzymy  aplikację  SMS  station.  Aplikacja  ma  symulować  pracę  stacji  smsowej.
Stwórz  klasę  SmsStation.  zakładamy  że  stacja  smsowa  otrzymuje j akieś  zlecenie  o  wysłaniu smsa,  które  musi  się  wydarzyć  poprzez  broadcast.  Ta  klasa  powinna  mieć  metodę setChanged i  notifyObservers,  oraz:
void  addPhone(Phone  p )  -   dodaje t elefon  do  stacji. void  sendSms(String  numer,  String t resc)
Phone j est  klasą  która  otrzymuje  smsy.  Każdy t elefon  ma  przypisany  numer t elefonu.
Metoda  sendSms t worzy  obiekt t ypu  Message,  a  następnie r ozsyła  go.  Klasa  Message posiada  dwa  pola ( tresc i  numer).  Obiekt  Message j est r ozsyłany,  a  następnie  wiadomości docierają  do  wszystkich t elefonów  w  pobliżu.  Sms  po t ym j ak  dotrze  do t elefonu, sprawdzamy  czy  powinien  dotrzeć  do t ego t elefonu ( sprawdzamy  czy  numer  z  wiadomości jest t aki j ak  numer  z t elefonu).
Użytkownik  z  poziomu  Main'a  wpisuje  komendę: 1235346457 j akas t resc  smsa
opcjonalnie  dodaj  komendę
addPhone  nr_tel alternatywnie  do  dodawania t elefonów  do  stacji  z l inii  poleceń,  możesz  dodać  kilka t elefonów w  konstruktorze  stacji.
Po  otrzymaniu  komendy  podziel j ą  przez  split  na  numer ( 1  słowo  komendy)  oraz  pozostałą część  komendy -  t o  będzie t reść  smsa.