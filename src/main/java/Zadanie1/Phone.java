package Zadanie1;

import java.util.Observable;
import java.util.Observer;

public class Phone implements Observer{

    private int phoneNumber;

    public Phone(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public void update(Observable o, Object arg) {
        if(arg instanceof Message && this.phoneNumber == ((Message) arg).getNumber()){
            System.out.println("Message: '" + ((Message) arg).getText() + "' has been received by Phone " + phoneNumber);
        }
        if (!(arg instanceof Message) && (this.phoneNumber == ((Message) arg).getNumber())) System.out.println("Wrong message format");
    }
}
